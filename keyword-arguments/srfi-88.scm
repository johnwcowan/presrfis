;; Implements the type and procedure library *only* of SRFI 88,
;; without the lexical notation, and without the hacks over the basic
;; symbol type used in the actual SRFI 88 sample implementation.
;;
;; Keywords are never garbage collected, but that could be fixed. A
;; real implementation would want to provide a lexical notation, of
;; course, and thus this whole module would likely be in the guts of
;; the interpreter rather than userland. This is just to show that ‘it
;; can be done’.
;;
;; Needs (only!) R7RS Red.

(import (scheme comparator))
(import (scheme hash-table))

(define keyword-name-comparator
  (make-comparator string?
                   string=? string<?
                   (lambda (str)
                     ;; Not quite djb2a.
                     (let ((hash 5381))
                       (string-for-each
                        (lambda (c)
                          (set! hash
                                (+ (* hash 33) (char->integer c))))
                        str)
                       (modulo hash (hash-bound))))))
(define *keyword-table* (make-hash-table keyword-name-comparator))

(define-record-type <keyword>
  (%make-keyword name)
  keyword?
  (name keyword->string))

(define (string->keyword keyword)
  (hash-table-intern! *keyword-table*
                      keyword
                      (lambda () (%make-keyword keyword))))

;; Convenience syntax. (: keyword) === (string->keyword "keyword")
(define-syntax :
  (syntax-rules ()
    ((_ kw) (string->keyword (symbol->string (quote kw))))))
