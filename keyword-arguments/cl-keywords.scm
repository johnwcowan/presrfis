;; Implements Common Lisp-style keywords (symbols with leading colons)
;; with the procedure library of DSSSL/SRFI 88.

(define (keyword? x)
  (and (symbol? x)
       (char=? (string-ref (symbol->string x) 0) #\:)))

(define (string->keyword s)
  (string->symbol (string-append ":" s)))

(define (keyword->string kw)
  (if (not (keyword? kw)) (error "not a keyword" kw)
      (substring (symbol->string kw) 1)))

;; Convenience syntax. (: keyword) === (string->keyword "keyword")
;; Theoretically, this could use Oleg Kiselyov’s trick for
;; constructing identifier names in syntax-rules, but I don’t
;; understand how that works and I’m not sure it’s well-specified.
(define-syntax :
  (syntax-rules ()
    ((_ kw) (string->keyword (symbol->string (quote kw))))))

