(import (scheme comparator)
        (scheme cxr)
        (scheme list))

(load "srfi-88.scm")

(define eqc (make-eq-comparator))
(define (keyword-hash kw) (comparator-hash eqc kw))

(load "explicit-renaming.scm")

(import (chibi))     ;; for er-macro-transformer
(import (chibi ast)) ;; for procedure-name-set!

(define-syntax lambda*
  (er-macro-transformer
   (lambda (form rename form=?)
     (let* ((formals-list (cadr form))
            (body (cddr form))
            (formals (parse-formals formals-list)))
       (apply expand-lambda* rename body formals)))))

(define-syntax define*
  (syntax-rules ()
    ((_ (name . formals) body ...)
     (begin
       (define name (lambda* formals body ...))
       ;; Chibi specific
       (procedure-name-set! name (quote name))))
    ((_ name value) (define name value))))
