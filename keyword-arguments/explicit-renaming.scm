;; Implements SRFI 89 for explicit renaming macros. Derived in part
;; from the SRFI 89 sample implementation, but hygienic.

(include "srfi-89-common.scm")
(define *keyword-not-given* (cons 'no-value '()))

(define (make-perfect-hash-table keywords)
  (let increase-size ((n (length keywords)))
    (let ((v (make-vector n #f)))
      (let try-hash ((remaining keywords))
        (if (null? remaining)
            (values v (lambda (kw) (modulo (keyword-hash kw) n)))
            (let* ((kw (car remaining))
                   (h (modulo (keyword-hash kw) n)))
              (if (vector-ref v h)
                  (increase-size (+ n 1))
                  (begin
                    (vector-set! v h kw)
                    (try-hash (cdr remaining))))))))))

(define (expand-optionals rename body positional-opts)
  (if (null? positional-opts) body
      `((,(rename 'let) ((,(caar positional-opts)
                         (,(rename 'if) (,(rename 'null?) ,(rename 'more))
                          ,(cadar positional-opts)
                          (,(rename 'car) ,(rename 'more))))
                         (,(rename 'more)
                          (,(rename 'if) (,(rename 'null?) ,(rename 'more))
                           ,(rename 'more)
                           (,(rename 'cdr) ,(rename 'more)))))
        ,@(expand-optionals rename body (cdr positional-opts))))))

(define (expand-named rename body table kw-hash named)
  (if (null? named)
      body
      `((,(rename 'let) ((,(rename 'kw-table) ',table)
                         (,(rename 'kw-hash) ',kw-hash)
                         (,(rename 'val-table)
                          (,(rename 'make-vector) ,(vector-length table)
                           ,(rename '*keyword-not-given*))))
         (,(rename 'let)
          ((,(rename 'more)
            (,(rename 'process-keywords)
             (,(rename 'lambda) (name value)
              (,(rename 'let) ((,(rename 'h) (,(rename 'kw-hash) name)))
               (,(rename 'cond) ((,(rename 'not) (,(rename 'eq?) name
                                                  (,(rename 'vector-ref)
                                                   ,(rename 'kw-table)
                                                   ,(rename 'h))))
                                 (,(rename 'error) "unknown keyword argument"
                                  name))
                ((,(rename 'eq?) (,(rename 'vector-ref) ,(rename 'val-table)
                                  ,(rename 'h))
                  ,(rename '*keyword-not-given*))
                 (,(rename 'vector-set!) ,(rename 'val-table)
                  ,(rename 'h)
                  value))
                (else (error "duplicate keyword argument" name)))))
             ,(rename 'more))))
          (,(rename 'let*) ,(map
                             (lambda (named-formal)
                               `(,(cadr named-formal)
                                 (,(rename 'let) ((,(rename 'actual-value)
                                                   (,(rename 'vector-ref)
                                                    ,(rename 'val-table)
                                                    (,(rename 'kw-hash)
                                                     ',(car named-formal)))))
                                  (,(rename 'if)
                                   (,(rename 'eq?)
                                    ,(rename 'actual-value)
                                    ,(rename '*keyword-not-given*))
                                   ,(if (null? (cddr named-formal))
                                        `(,(rename 'error)
                                          "required keyword argument missing"
                                          ',(car named-formal))
                                        (caddr named-formal))
                                   ,(rename 'actual-value)))))
                             named)
           ,@body))))))

(define (process-keywords proc list)
  (if (and (not (null? list))
           (keyword? (car list)))
      (begin
        (proc (car list) (cadr list))
        (process-keywords proc (cddr list)))
      list))

(define (expand-rest rename body rest)
  (if rest
      `((,(rename 'let) ((,rest ,(rename 'more)))
         ,@body))
      `((,(rename 'if) (,(rename 'null?)
                        ,(rename 'more))
         (,(rename 'begin) ,@body)
         (,(rename 'error)
          "excess arguments passed"
          ,(rename 'more))))))

(define (expand-lambda* rename
                        body
                        positional-before-named?
                        positional-reqs
                        positional-opts
                        named
                        rest)
  (let-values (((table kw-hash) (make-perfect-hash-table
                                 (map car named))))
    (if positional-before-named?
        `(,(rename 'lambda) ,(append positional-reqs (rename 'more))
          ,@(expand-optionals
             rename
             (expand-named rename
                           (expand-rest rename body rest)
                           table
                           kw-hash
                           named)
             positional-opts))
        `(,(rename 'lambda) ,(rename 'more)
          ,@(expand-named rename
                          `((,(rename 'apply)
                             (,(rename 'lambda*)
                              ,(append positional-reqs
                                       positional-opts
                                       (if rest rest '()))
                              ,@body)
                             ,(rename 'more)))
                          table
                          kw-hash
                          named)))))

;;;; SRFI 89 Sample Implementation Copyright Notice
;;
;; Copyright (C) Marc Feeley (2006). All Rights Reserved.
;;
;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.
