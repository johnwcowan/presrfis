;; Colon macro which generates keyword at compile time and assumes
;; that they are self-evaluating.
;;
;; Despite this, it does not work for calling keyword procedures in
;; Kawa or Racket.

(define-syntax :
  (lambda (stx)
    (syntax-case stx ()
      ((_ kw) (datum->syntax stx (string->keyword
                                  (symbol->string
                                   (syntax->datum #'kw))))))))
