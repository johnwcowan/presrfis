This tree contains various experiments with keyword arguments, mostly related to SRFI 89, mostly obsolete.

REVIEW.md which was formerly here has been moved to a different branch ([stable](https://gitlab.com/dpk/presrfis/-/tree/pages), [unstable](https://gitlab.com/dpk/presrfis/-/tree/keywords-draft)).
