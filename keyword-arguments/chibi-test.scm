;;

(import
 (scheme base)
 (scheme load)
 (chibi test))

(load "chibi.scm")

;; f example from SRFI 89
(test-group "optional parameters"
  (define* (f a (b #f)) (list a b))
  (test "omitted argument"
    '(1 #f) (f 1))
  (test "all arguments given"
    '(1 2) (f 1 2))
  (test-error "too many arguments"
    (f 1 2 3)))

;; g example from SRFI 89
(test-group "optional and named parameters with dependent defaults"
  (define* (g a (b a) ((: key) k (* a b))) (list a b k))
  (test "omitted positional argument"
    '(3 3 9) (g 3))
  (test "omitted keyword argument"
    '(3 4 12) (g 3 4))
  (test-error "keyword with no value"
    (g 3 4 (: key)))
  (test "all arguments given"
    '(3 4 5) (g 3 4 (: key) 5))
  (test-error "bad keyword given"
    (g 3 4 (: zoo) 5))
  (test-error "duplicate keyword given"
    (g 3 4 (: key) 5 (: key) 6)))

;; h1 example from SRFI 89
(test-group "keyword argument at end of params list with rest"
  (define* (h1 a ((: key) k #f) . r) (list a k r))
  (test "omitted keyword argument, no rest"
    '(7 #f ()) (h1 7))
  (test "omitted keyword argument with rest"
    '(7 #f (8 9 10)) (h1 7 8 9 10))
  (test "keyword argument with rest"
    '(7 8 (9 10)) (h1 7 (: key) 8 9 10))
  (test-error "bad keyword given"
    (h1 7 (: key) 8 (: zoo) 9)))

;; h2 example from SRFI 89
(test-group "keyword argument at start of params list with rest"
  (define* (h2 ((: key) k #f) a . r) (list a k r))
  (test "omitted keyword argument, no rest"
    '(7 #f ()) (h2 7))
  (test "omitted keyword argument with rest"
    '(7 #f (8 9 10)) (h2 7 8 9 10))
  (test "keyword argument with rest"
    '(9 8 (10)) (h2 (: key) 8 9 10))
  (test-error "bad keyword given"
    (h2 (: key) 8 (: zoo) 9)))

