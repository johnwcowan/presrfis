;; Procedural helpers from the SRFI 89 sample implementation. Since we
;; prefer hygienic macro systems over define-macro, the rest of
;; SRFI 89’s sample implementation isn’t used here.

;; One change to the original to support Schemes without lexical
;; notation for keywords (and for interoperability between Schemes
;; with different ideas of how to spell keywords): ~(: name)~ is
;; accepted in place of SRFI 88 ~name:~.
(define (keywordish? x)
  (or (keyword? x)
      (and (pair? x)
           (eq? (car x) ':)
           (pair? (cdr x))
           (symbol? (cadr x))
           (null? (cddr x)))))

(define (keywordish->keyword x)
  (if (keyword? x) x
      (string->keyword (symbol->string (cadr x)))))

(define (variable? x) (symbol? x))

(define (required-positional? x)
  (variable? x))

(define (optional-positional? x)
  (and (pair? x)
       (pair? (cdr x))
       (null? (cddr x))
       (variable? (car x))))

(define (required-named? x)
  (and (pair? x)
       (pair? (cdr x))
       (null? (cddr x))
       (keywordish? (car x))
       (variable? (cadr x))))

(define (optional-named? x)
  (and (pair? x)
       (pair? (cdr x))
       (pair? (cddr x))
       (null? (cdddr x))
       (keywordish? (car x))
       (variable? (cadr x))))

(define (named? x)
  (or (required-named? x)
      (optional-named? x)))

(define (duplicates? lst)
  (cond ((null? lst)
         #f)
        ((memq (car lst) (cdr lst))
         #t)
        (else
         (duplicates? (cdr lst)))))

(define (parse-positional-section lst cont)
  (let loop1 ((lst lst) (rev-reqs '()))
    (if (and (pair? lst)
             (required-positional? (car lst)))
        (loop1 (cdr lst) (cons (car lst) rev-reqs))
        (let loop2 ((lst lst) (rev-opts '()))
          (if (and (pair? lst)
                   (optional-positional? (car lst)))
              (loop2 (cdr lst) (cons (car lst) rev-opts))
              (cont lst (cons (reverse rev-reqs) (reverse rev-opts))))))))

(define (parse-named-section lst cont)
  (let loop ((lst lst) (rev-named '()))
    (if (and (pair? lst)
             (named? (car lst)))
        (loop (cdr lst) (cons (cons (keywordish->keyword (caar lst))
                                    (cdar lst)) rev-named))
        (cont lst (reverse rev-named)))))

(define (parse-rest lst
                    positional-before-named?
                    positional-reqs/opts
                    named)
  (if (null? lst)
      (parse-end positional-before-named?
                 positional-reqs/opts
                 named
                 #f)
      (if (variable? lst)
          (parse-end positional-before-named?
                     positional-reqs/opts
                     named
                     lst)
          (error "syntax error in formal parameter list"))))

(define (parse-end positional-before-named?
                   positional-reqs/opts
                   named
                   rest)
  (let ((positional-reqs (car positional-reqs/opts))
        (positional-opts (cdr positional-reqs/opts)))
    (let ((vars
           (append positional-reqs
                   (map car positional-opts)
                   (map cadr named)
                   (if rest (list rest) '())))
          (keys
           (map car named)))
      (cond ((duplicates? vars)
             (error "duplicate variable in formal parameter list"))
            ((duplicates? keys)
             (error "duplicate keyword in formal parameter list"))
            (else
             (list positional-before-named?
                   positional-reqs
                   positional-opts
                   named
                   rest))))))

(define (parse-formals lst)
  (if (and (pair? lst)
           (named? (car lst)))
      (parse-named-section
       lst
       (lambda (lst named)
         (parse-positional-section
          lst
          (lambda (lst positional-reqs/opts)
            (parse-rest lst
                        #f
                        positional-reqs/opts
                        named)))))
      (parse-positional-section
       lst
       (lambda (lst positional-reqs/opts)
         (parse-named-section
          lst
          (lambda (lst named)
            (parse-rest lst
                        #t
                        positional-reqs/opts
                        named)))))))

;;;; Original SRFI 89 Sample Implementation Copyright Notice
;;
;; Copyright (C) Marc Feeley (2006). All Rights Reserved.
;;
;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.
