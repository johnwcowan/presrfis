(import (only (srfi 69) eq?-hash))
(import (only (srfi 1) every))
(import (chicken keyword))

;; Use a dual implementation strategy. If the argument list given is
;; compatible with Chicken’s implementation of DSSSL-style keyword
;; arguments, lambda* and define* just convert the SRFI 89-ish input
;; to DSSSL style and do a minimal runtime conversion to handle
;; Chicken’s deviant behaviour for #!rest arguments. Otherwise, fall
;; back to our own implementation.
(define (dsssl-compatible? positional-before-named?
                           positional-reqs
                           positional-opts
                           named
                           rest)
  (and positional-before-named?
       (every (lambda (named-formal)
                (let ((keyword-name (car named-formal))
                      (binding-name (cadr named-formal)))
                  (eq? (string->symbol (keyword->string keyword-name))
                       binding-name)))
              named)))

(define (formals->dsssl positional-before-named?
                        positional-reqs
                        positional-opts
                        named
                        rest)
  (append positional-reqs
          (if (not (null? positional-opts))
              (append (list '#!optional) positional-opts)
              '())
          (if rest
              (list '#!rest rest)
              '())
          (if (not (null? named))
              (append (list '#!key)
                      (map (lambda (named-formal)
                             (let* ((keyword-name (car named-formal))
                                    (binding-name (cadr named-formal))
                                    (default-value
                                      (if (not (null? (cddr named-formal)))
                                          (caddr named-formal)
                                          `(error "missing keyword argument"
                                                  ',keyword-name))))
                               (list binding-name default-value)))
                           named))
              '())))

;; Chicken’s #!rest includes all the keyword arguments and their
;; values. Drop them.
(define (drop-keywords reals)
  (cond ((null? reals) reals)
        ((keyword? (car reals)) (drop-keywords (cddr reals)))
        (else reals)))

(define keyword-hash eq?-hash)
(include "explicit-renaming.scm")

(define-syntax lambda*
  (er-macro-transformer
   (lambda (form rename form=?)
     (let* ((formals-list (cadr form))
            (body (cddr form))
            (formals (parse-formals formals-list)))
       (if (apply dsssl-compatible? formals)
           `(,(rename 'lambda)
             ,(apply formals->dsssl formals)
             ,@(if (list-ref formals 4)
                   `((,(rename 'let) ((,(list-ref formals 4)
                                        (,(rename 'drop-keywords)
                                         ,(list-ref formals 4))))
                       ,@body))
                   body))
           (apply expand-lambda* rename body formals))))))

(define-syntax define*
  (syntax-rules ()
    ((_ (name . formals) body ...)
     (define name (lambda* formals body ...)))
    ((_ name value) (define name value))))

;; The convenience syntax from the portable SRFI 88 implementation is
;; provided for interoperability.
(define-syntax :
  (syntax-rules ()
    ((_ kw) (string->keyword (symbol->string (quote kw))))))
