# `syntax-case` Extensions

## Rationale

One of the most common criticisms of the `syntax-case` macro system as adopted by the R6RS relate to the pattern matcher. While it gives `syntax-case` macros a certain basic familiarity for users of the `syntax-rules` system, and in most cases pattern matching is the most convenient way to destructure the arguments of a procedural macro, it is admittedly unusual for Scheme to provide such a high-level interface to destructuring a datum without also exposing the underlying primitives. This (Pre-)SRFI provides a set of procedures to inspect syntax objects without the direct use of the pattern matcher, which may be useful for those who wish to implement their own destructuring system for procedural hygienic macros. The majority of these have also previous been available in at least some Schemes with `syntax-case`, though no single Scheme implementation at time of writing provides the complete set specified here.

In addition, some common restrictions on uses of macros are accommodated poorly or not at all by pure pattern matching. The `syntax-case` system’s provision of guard clauses allowing the matching of patterns to be subject to secondary checks by arbitrary Scheme code makes a basic provision for enforcing these restrictions. This (Pre-)SRFI also provides predicates and predicate combiners intended to be useful for ensuring the correct use of macros in guard clauses and similar.

Finally, to aid in writing macros which perform constant folding or similar optimizations, a procedure is provided which allows eagerly expanding syntax objects within a syntax transformer.

## Specification

### Predicates

#### `(stx-pair? stx)` (Procedure)

Returns `#t` if `#stx` is a syntax object encapsulating a pair, and `#f` otherwise.

Portable implementation:

```scheme
(define (stx-pair? stx)
  (syntax-case stx ()
    ((_ . _) #t)
    (_ #f)))

;; (stx-pair? #'(1 . 2)) ;;=> #t
;; (stx-pair? #'(x y))   ;;=> #t
;; (stx-pair? #'())      ;;=> #f
;; (stx-pair? #'x)       ;;=> #f
;; (stx-pair? 'not-stx)  ;;=> #f
```

#### `(stx-null? stx)` (Procedure)

Returns `#t` if `stx` is a syntax object encapsulating the empty list, and `#f` otherwise.

Portable implementation:

```scheme
(define (stx-null? stx)
  (syntax-case stx ()
    (() #t)
    (_ #f)))

;; (stx-null? #'())      ;;=> #t
;; (stx-null? '())       ;;=> #t
;; (stx-null? #'(a . b)) ;;=> #f
```

#### `(stx-list? stx)` (Procedure)

Returns `#t` if `stx` is a syntax object encapsulating a proper list, and `#f` otherwise.

Portable implementation:

```scheme
(define (stx-list? stx)
  (syntax-case stx ()
    ((_ ...) #t)
    (_ #f)))

;; (stx-list? #'(x y))   ;;=> #t
;; (stx-list? #'())      ;;=> #t
;; (stx-list? #'(x . y)) ;;=> #f
;; (stx-list? #'x)       ;;=> #f
```

#### `(stx-dotted-list? stx)` (Procedure)

Returns `#t` if `stx` is a syntax object encapsulating an improper list in the sense of [SRFI 1][srfi-1], and `#f` otherwise.

Portable implementation:

```scheme
(define (stx-dotted-list? stx)
  (not (or (stx-null? stx)
           (stx-list? stx))))

;; (stx-dotted-list? #'(x y . z)) ;;=> #t
;; (stx-dotted-list? #'x)         ;;=> #t
;; (stx-dotted-list? #'(x y z))   ;;=> #f
;; (stx-dotted-list? #'())        ;;=> #f
```

[srfi-1]: https://srfi.schemers.org/srfi-1/srfi-1.html

No `stx-circular-list?` is provided because circular lists are illegal in Scheme code anyway.

#### `(stx-vector? stx)` (Procedure)

Returns `#t` if `stx` is a syntax object encapsulating a vector, and `#f` otherwise.

Portable implementation:

```scheme
(define (stx-vector? stx)
  (syntax-case stx ()
    (#(_ ...) #t)
    (_ #f)))
```

#### `(stx-constant? stx)` (Procedure)

Returns `#t` if `stx` is a syntax object encapsulating a constant value, and `#f` otherwise.

TODO: Should this return `#t` on `quote`d forms?

The following portable implementation requires adaptation on Schemes which define other self-evaluating data types:

```scheme
(define (stx-constant? stx)
  (let ((d (syntax-e stx)))
    (or (number? d)
        (char? d)
        (string? d)
        (vector? d)
        (bytevector? d))))
```

#### `(stx-every pred l)` (Procedure)

`l` must be a list of identifiers or a syntax object encapsulating a list of identifiers. If the list is dotted, the final cdr is treated as if it were the final element of a proper list.

Applies `pred` to the elements of `l` in left-to-right order, returning `#f` immediately if the application to any element returned `#f`; otherwise returns the result of calling `pred` on the final element of `l`. Returns `#t` if `l` is an empty list.

This may be used in guard clauses to ensure that everything matched by a dotted tail or ellipsis pattern is of the correct type.

Portable implementation:

```scheme
(define (stx-dotted->proper l)
  (syntax-case l ()
    ((xs ...) l)
    ((xs ... . y) #'(xs ... y))))

(define (stx-every pred l)
  (let loop ((l* (stx-dotted->proper l)))
    (syntax-case l* ()
      (() #t)
      ((x) (pred #'x))
      ((x . ys)
       (and (pred #'x)
            (loop #'ys))))))

;; (stx-every identifier? #'(a b c))   ;;=> #t
;; (stx-every identifier? #'(a (b c))  ;;=> #f
;; (stx-every identifier? #'(a b . c)) ;;=> #t
;; (stx-every identifier? #'a)         ;;=> #t
```

#### `(stx-any pred l)` (Procedure)

`l` must be a list of identifiers or a syntax object encapsulating a list of identifiers. If the list is dotted, the final cdr is treated as if it were the final element of a proper list.

Applies `pred` to the elements of `l` in left-to-right order, immediate returning the result of calling `pred` on the first element which does not return `#f`; otherwise returning `#f` if `pred` called on all the elements in `l` returns `#f`. Also returns `#f` if `l` is an empty list.

This may be used in guard clauses to ensure that one item in a list matched by an ellipsis or dotted pattern is of the correct type or matches a given literal.

Portable implementation:

```scheme
(define (stx-any pred l)
  (let loop ((l* (stx-dotted->proper l)))
    (syntax-case l* ()
      (() #f)
      ((x) (pred #'x))
      ((x . ys)
       (or (pred #'x)
           (loop #'ys))))))

;; (stx-any (lambda (v) (free-identifier=? v #'_)) #'(< _ y)) ;;=> #t
;; (stx-any (lambda (v) (free-identifier=? v #'_)) #'(< x y)) ;;=> #f
```

#### `(body? stx)` (Procedure)

Returns `#t` if `stx` is a syntax object containing a list which would be a valid body of a `lambda` expression; or `#f` if it is any other object. Intended to be useful in guard clauses to replace the awkward construct `body1 body2 ...` (or however one prefers to spell it). However, in contrast to that pattern matcher, this procedure is allowed, but not required, to enforce other restrictions within the top level of the sequence of expressions of the body, such as on the placement of `define` or on literals such as `()` which it is an error to evaluate, by returning `#f` when called with a syntax list containing such constructs. It should not recurse more than one level into the syntax list to enforce such restrictions, however.

Portable implementation:

```scheme
(define (body? stx)
  (syntax-case stx ()
    ((_ _ ...) #t)
    (_ #f)))
```

<!-- examples -->
This can be used as a guard in `syntax-case` patterns with dotted tail body matchers:

```scheme
(define-syntax my-lambda
  (lambda (stx)
    (syntax-case stx ()
      ((_ args . body)
       (body? #'body)
       #'(lambda args . body)))))
```

Or with ellipsis matchers:

```scheme
(define-syntax my-lambda
  (lambda (stx)
    (syntax-case stx ()
      ((_ args body ...)
       (body? #'(body ...))
       #'(lambda args body ...)))))
```
<!-- /examples -->

### `(distinct-identifiers? l)`

`l` must be a list of identifiers or a syntax object encapsulating a list of identifiers. If the list is dotted, the final cdr must be an identifier, which is treated as if it were the final element of a proper list.

Return `#t` if the identifiers in the list are all distinct in the sense of `bound-identifier=?` and `#f` otherwise. This can be used to ensure that a list of procedure formals does not contain any ambiguous repeated names.

(Extremely inefficient) portable implementation:

```scheme
(define (distinct-identifiers? l)
  (let loop ((l* (stx-dotted->proper l)))
    (syntax-case l* ()
      (() #t)
      ((id) (identifier? #'id) #t)
      ((id1 id2) (not (bound-identifier=? #'id1 #'id2)))
      ((id1 id2 . more-ids)
       (and (not (bound-identifier=? #'id1 #'id2))
            (stx-every (lambda (idn) (not (bound-identifier=? #'id1 idn)))
                       #'more-ids)
            (loop #'(id2 . more-ids)))))))

;; (distinct-identifiers? #'(x y z))    ;;=> #t
;; (distinct-identifiers? #'(x y . z))  ;;=> #t
;; (distinct-identifiers? #'(x y x))    ;;=> #f
;; (distinct-identifiers? #'(x y . x))  ;;=> #f
;; (distinct-identifiers? (generate-temporaries #'(x x))) ;;=> #t
```

TODO: Should this take multiple lists and ensure that all identifiers in all lists are all distinct? This would make things like handling duplicate names in `opt-lambda` and similar (where identifiers may ultimately have come from distinct patterns) simpler.

### Unwrappers

#### `(syntax-e stx)` (Procedure)

Unpacks only one level of the syntax object `stx` as if with `syntax->datum`, so that e.g. an identifier is unpacked to a symbol, but an expression is expanded to a cons pair containing two further syntax objects.

Portable implementation:

```scheme
(define (syntax-e stx)
  (syntax-case stx ()
    ((a . b) (cons #'a #'b))
    (() '())
    (#(x ...) (list->vector (syntax->list #'(x ...))))
    ;; assume everything else is a constant or identifier
    (etc (syntax->datum #'etc))))

;; (syntax-e #'(x y z)) ;;=> (#'x . #'(y z))
;; (syntax-e #'())      ;;=> ()
;; (syntax-e #'#(x y))  ;;=> #(#'x #'y)
```

#### `(stx-car stx)` (Procedure)

Returns the car of the pair encapsulated by the syntax object `stx` as a syntax object.

Portable implementation:

```scheme
(define (stx-car stx)
  (syntax-case stx ()
    ((a . _) #'a)))
```

#### `(stx-cdr stx)` (Procedure)

Returns the cdr of the pair encapsulated by the syntax object `stx` as a syntax object.

Portable implementation:

```scheme
(define (stx-cdr stx)
  (syntax-case stx ()
    ((_ . b) #'b)))
```

#### `(syntax->list stx)` (Procedure)

Unpacks an expression into a list of syntax objects, as if by calling `syntax-e` on the successive cdrs of its output until the end of the list.

Portable implementation:

```scheme
(define (syntax->list stx)
  (cond ((stx-pair? stx)
         (cons (stx-car stx) (syntax->list (stx-cdr stx))))
        ((stx-null? stx) '())
        (else stx)))
```

### Comparators

These comparators may be used, for example, to build mappings, hash tables, and other data structures with identifiers as keys. Note that data structures built with `free-identifier-comparator` must be used with care, as the results of comparisons can change if the binding of an identifier changes between storing it in a data structure and comparing it with another while attempting to retrieve it, causing unexpected lookup failures.

TODO: The specs and sample implementations below assume that identifiers can be `free-identifier=?` even if they wrap different symbols (because of import renaming and aliasing etc.), but `bound-identifier=?` can only be true if the identifiers have the same symbolic name. This is pushing the boundaries of my understandings of the edge cases of these comparison procedures: is this correct?

#### `free-identifier-comparator` (Comparator)

A comparator whose type test is `identifier?`, whose equality predicate is `free-identifier=?`, and whose ordering predicate and hash function may be provided but are not guaranteed to exist. The ordering predicate, if present, sorts identifiers into an implementation-defined order, which must provide consistent interaction with the equality predicate for the purposes of less-than-or-equal-to and greater-than-or-equal-to comparisons.

(Very poor) portable implementation:

```scheme
(define free-identifier-comparator
  (make-comparator identifier?
                   free-identifier=?
                   #f
                   #f))
```

#### `bound-identifier-comparator` (Comparator)

A comparator whose type test is `identifier?`, whose equality predicate is `bound-identifier=?`, whose ordering predicate may be provided but is not guaranteed to exist, and whose hash function must be at least as granular as calling SRFI 128 `symbol-hash` on the symbolic name of the identifier. The ordering predicate, if present, sorts identifiers into an implementation-defined order, which must provide consistent interaction with the equality predicate for the purposes of less-than-or-equal-to and greater-than-or-equal-to comparisons.

(Poor) portable implementation:

```scheme
(define bound-identifier-comparator
  (make-comparator identifier?
                   bound-identifier=?
                   #f
                   (lambda (id) (symbol-hash (syntax->datum id)))))
```

### Procedure for writing optimizing macros

#### `(syntax-expand stx)` (Procedure)

Perform syntax expansion on the expression encapsulated by the `stx` syntax object and return a new syntax object containing the macro-expanded code suitable for embedding into the output of another syntax transformer. Allows writing eagerly-expanding macros, which may be useful for optimization or similar.

However, there is no guarantee about what the syntax object returned by `syntax-expand` will look like when examined. In particular, `syntax-expand` need not perform total expansion. If `syntax-expand` determines that `stx` is already as fully-expanded as `syntax-expand` can manage (which may be less than the ultimate expansion produced by the compiler), it can return `stx` itself instead of copying it to a new syntax object.

Since `syntax-expand` can do *total* macro expansion, the resulting syntax object can be marked as fully expanded, and does not need to be processed again by the expander if it is embedded inside another macro-expansion (though it would have no effect if it were, other than making macro expansion take longer).

Supported by Racket as `(local-expand stx 'expression '())`. See the [Racket documentation.](https://docs.racket-lang.org/reference/stxtrans.html#%28def._%28%28quote._~23~25kernel%29._local-expand%29%29)

Since there is no guarantee about the depth of expansion performed by `syntax-expand`, the identity function is a valid portable implementation of this procedure.
<!--
```scheme
(define (syntax-expand stx) stx)
```
--->
```scheme
;; (syntax-expand #'(let ((x 1)) (+ x 2)))
;;   ;;=> #'(let ((x 1)) (+ x 2))
;; or
;;   ;;=> #'((lambda (x) (+ x 2)) 1)
;; or even
;;   ;;=> #'3
```

## Implementation

This entire library can be portably implemented by (ab)using the `syntax-case` pattern matcher to simulate the underlying primitive deconstructors. The implementations given for each procedure here will work on any Scheme implementing `syntax-case`, although `stx-constant?` only considers data types defined by R7RS small and will need adapting for R6RS and for most real Scheme implementations. The portable implementations are, of course, certainly slower than if the procedures were implemented by just exposing the primitive unwrappers of syntax objects.

## Acknowledgements

The vast majority of procedures provided here were implemented first in [Racket](https://docs.racket-lang.org/reference/Macros.html) and in [Gerbil](https://cons.io/reference/core-expander.html). Some are also implemented by [Chez](https://cisco.github.io/ChezScheme/csug9.5/syntax.html). (Some procedures have been renamed from these sources.) A number of procedures are obviously inspired by SRFI 1. The comparators were inspired by [Racket’s ‘Dictionaries with Identifier Keys’](https://docs.racket-lang.org/syntax/syntax-helpers.html#%28part._idtable%29).

`body?` was inspired by the author’s own personal distaste for the common `body1 body2 ...` pattern-matching construct. `distinct-identifiers?` was inspired by a remark on possible applications of `bound-identifier=?` the Appendix on Macros to the R4RS, and the author’s own experiences writing `lambda`- and `let`-like macros.
