;; SRFI 1 (list library) established the convention that the
;; accumulator passed to the kons argument of a fold procedure is the
;; last argument, not the first. This convention is unfortunate, but
;; we’re stuck with it: the vast majority of data-structure fold
;; procedures in Scheme follow this convention.
;;
;; Not all, though: SRFI 43 (vector library) broke this convention and
;; made the accumulator the *first* argument to kons; subsequent
;; libraries for vector-like things have followed suit. If you want to
;; use fold usefully as an argument to a higher-order procedure, this
;; inconsistency is extremely irritating.
;;
;; This is a hack to detect which version of kons a particular fold
;; procedure expects and convert it to a SRFI 1 style fold procedure.
;; It needs one or more non-empty example sequence arguments, but if
;; you always provide the argument(s) you’re actually going to fold
;; over, it will do the right thing even if one of them is empty.

(define *accumulator-sentinel* (cons 'acc '()))

(define (fixed-fold-proc fold-proc . seqs)
  (call-with-current-continuation
   (lambda (ret)
     (apply
      fold-proc
      (lambda fold-args
        (cond ((eq? (first fold-args) *accumulator-sentinel*)
               (ret (lambda (kons knil . fold-seqs)
                      (apply fold-proc
                             (lambda fold-proc-args
                               (apply kons
                                      (append (cdr fold-proc-args)
                                              (list (car fold-proc-args)))))
                             knil fold-seqs))))
              (else (ret fold-proc))))
      *accumulator-sentinel*
      seqs)
     ;; Handle the case where one of the seqs is empty and `ret`
     ;; didn’t get called above.
     (lambda (kons knil . rest) knil))))
