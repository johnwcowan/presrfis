This tree contains a quick-and-dirty portable implementation of applicable record types. `define-applicable-record-type` is, in theory, exactly like `define-record-type` except that the last argument is an expression which to create a procedure which is called with the record instance as its only argument when the record type is applied/called like a procedure itself. This procedure should in turn return another procedure which will receive the actual arguments the record type was called with. Also, defined applicable record types are not disjoint from all other types, because their instances all answer `#t` to `procedure?` — this is by design, for portability.

```scheme
(define-applicable-record-type <pare>
  (kons a b)
  pare?
  (a kar set-kar!)
  (b kdr set-kdr!)
  (lambda (self)
    (lambda (c)
      (list (kar self) (kdr self) c))))
      
(define my-pare (kons 1 2))
(kar my-pare) ;;=> 1
(kdr my-pare) ;;=> 2
(set-kar! my-pare 0)
(kar my-pare) ;;=> 0

(my-pare 4) ;;=> (0 2 4)
```

## Things that are wrong and/or not supported

Compared to `define-record-type`:

- There is no inheritance.
- The type name (the first argument) is currently ignored and not defined. Since it is meaningless in `define-record-type` anyway, this is not really important.
- The predicate procedure name is currently ignored and not defined. The plan is to use a weak hash table to map record instances to type names and the predicate will check in there.
- Field names are symbols, not identifiers as clarified by [SRFI 136](https://srfi.schemers.org/srfi-136/srfi-136.html). In the absence of inheritance, this makes no difference, but will have to be considered in the future.
- There is no checking for errors in definition forms or in your use of field accessor and setter procedures.

These should all be fixable. As mentioned, this is a quick-and-dirty version.
