# Syntax-based keyword procedures

`define-with-keywords` provides a SRFI 89-like way to define procedure-like syntax (periphrastic macros) that take keyword arguments in pure R7RS small. The macros created behave somewhat like Python’s keyword argument functions in that keyword arguments can also be specified positionally, by giving them in the order they appear in the definition. The difference is that arguments can be positional-only but not keyword-only — in other words, it supports the equivalent of Python’s `/` operator in function definitions, but not the equivalent of the (bare) `*` operator. This is mainly because of limitations of `syntax-rules` (keyword names cannot automatically be generated from binding names).

```scheme
(define-with-keywords (lyst a (k: k) (l: l (* k 2)))
  (list a k l))

(lyst 1)      ;;=> ERROR: required keyword argument missing, k:
(lyst 1 k: 2) ;;=> (1 2 4)
(lyst 1 k: 2 l: 3) ;;=> (1 2 3)
(lyst 1 l: 2 k: 3) ;;=> (1 3 2)

;; Providing keyword arguments positionally.
(lyst 1 2)   ;;=> (1 2 4)
(lyst 1 2 3) ;;=> (1 2 3)
```

The above examples work in the implementation given here on Chibi, Chicken (with `(keyword-syntax #f)`), Gauche, and Guile. It doesn’t work in Gerbil, probably firstly because `k:` and `l:` get parsed as keyword datums and not as identifiers as the code expects, but even if you change them to e.g. `:k` and `:l` it doesn’t like the expansion of `define-with-keywords` for an unknown reason. It won’t work natively on R6RS implementations that don’t support the extended `syntax-rules` of R7RS, but I’m told it works with [this](../r7rs-syntax-rules.scm) wrapper around `syntax-rules` in at least Chez.

`define-with-keywords` as used above actually defines macros; that is, `lyst` has no first-class status. You can pass an extra identifier as the first argument to `define-with-keywords` and it will be bound to a real, first-class procedure taking the same required and optional arguments, but only positionally:

```scheme
(define-with-keywords lyst-proc (lyst a (k: k) (l: l (* k 2)))
  (list a k l))

;; `lyst` itself will work as above, but it’s also possible to do
;; things like this with `lyst-proc`:
(map lyst-proc '(1 2 3) '(4 5 6)) ;; => ((1 4 8) (2 5 10) (3 6 12))
```

## Specification

*Syntax:*

```scheme
(define-with-keywords [proc-name]
                (name positional-arg*
                      (required-keyword-name required-keyword-binding)*
                      (optional-keyword-name optional-keyword-binding initializer)*)
  body*)
```

`proc-name` (if given), `name`, and all of the `positional-arg`, `required-keyword-name`, `required-keyword-binding`, `optional-keyword-name`, and `optional-keyword-binding` are identifiers. `initializer`s can be any expression. `body` has the same meaning as in a `lambda` expression.

Note that all the required keyword arguments have to come before the optional keyword arguments in the formal parameters of the macro. (This does not affect the order in which keyword arguments may be passed to the defined macro.) Also, periphrastic macros created with `define-with-keywords` can only have a fixed number of positional arguments and no rest arguments.

*Semantics:* Binds the syntax keyword `name` to a syntax transformer that acts like a procedure receiving the given positional arguments, followed by keyword arguments. The given `required-keyword-name`s and `optional-keyword-name`s become bindings to auxiliary syntax keywords, using [SRFI 206][srfi-206] if possible. (This implementation does not use SRFI 206.)

[srfi-206]: https://srfi.schemers.org/srfi-206/srfi-206.html

If `proc-name` is given, it is bound as if by `define` to a real first-class procedure receiving the same arguments as the macro defined under `name`, but which can only receive these arguments positionally.

When the defined `name` is invoked, the given positional arguments are evaluated as normal and bound within the `body` to the names of the `positional-arg`s. If there are any further positional arguments not associated with the `required-keyword-name` or `optional-keyword-name` auxiliary syntax keywords, these are bound to the names of the given `required-keyword-binding`s and `optional-keyword-binding`s within the body, in the order that they were defined. Then the remainder of the arguments passed are interpreted as a property list, matching the auxiliary syntax keywords for the `required-keyword-name`s and `optional-keyword-name`s to the values that should be bound to the corresponding  `required-keyword-binding`s and `optional-keyword-binding`s. Finally, if there are any `optional-keyword-name`s in the formal parameters of the periphrastic which were not given when invoked, then they are processed from left to right in the order they appear in the formal parameters, being bound using the given `initializer` expressions with the bindings of the `positional-arg`s, the `required-keyword-binding`s, and *only* the `optional-keyword-binding`s which appear to the *left* of the formal specification of the parameter in scope. Then `body` is evaluated with all the `positional-arg`s, `required-keyword-binding`s, and `optional-keyword-binding`s within its lexical scope, as in a normal procedure call.

When invoking the macro defined under `name`, it is an error:

- to attempt to provide by a keyword binding a value to a keyword parameter which has already been passed positionally (this implementation will prefer the positionally-given value);
- to attempt to provide two keyword bindings for the same keyword parameter (this implementation will take the last one given);
- if, after one keyword binding has been given, additional positional parameters appear not following the strict property-list pattern (this implementation will treat any additional values after a keyword binding as additional positional parameters);
- to fail to provide a required keyword binding (this implementation will trigger a `syntax-error`).

(The lax behaviour of this implementation can be fixed to signal an error in each case. In some cases this would be easy, in other cases less so.)

## Suggested Behaviour on Schemes with `syntax-case` etc.

(Not actually implemented yet.)

On Schemes with R6RS `syntax-case`, assuming there is no existing keyword argument system with which interoperability is desired (see below):

- `name` is bound to a variable transformer that returns the same procedure as `proc-name` is (or would be) bound to when not used in operator position.
- Each of the `required-keyword-name`s and `optional-keyword-name`s is bound to immutable identifier syntax which simply returns the identifier (as a syntax object) of the syntax object. (In other words, `(define-syntax my-keyword: (identifier-syntax #'my-keyword:))`) (Issue: Would a `define-keyword` which does this automatically be useful?)
- The procedure `proc-name` would be bound to is able to handle arguments passed as keyword arguments in property-list style using the identifiers to which the names are bound, by doing keyword binding dynamically at run time.

As mentioned above, the keyword names should be defined with SRFI 206 `define-auxiliary-syntax` when possible.

If [SRFI 213][srfi-213] is available, some kind of identifier property should be assigned to the  `required-keyword-name`s and `optional-keyword-name`s identifying it as a keyword. A `keyword?` procedure should be provided returning `#t` if called on an identifier which has this identifier property, and `#f` otherwise. `keyword=?` is effectively an alias for `free-identifier=?`, but may return `#f` on identifiers which are not keywords.

[srfi-213]: https://srfi.schemers.org/srfi-213/srfi-213.html

## Suggested Compatibility Behaviour on Schemes with Existing Systems for Keyword Arguments

(Not actually attempted on any Scheme yet.)

This section pre-supposes identifier syntax features essentially equivalent to those provided by R6RS’s `syntax-case`, and modifies the suggestions above. It also supposes vaguely [SRFI 88][srfi-88]/DSSSL-like keyword objects.

[srfi-88]: https://srfi.schemers.org/srfi-88/srfi-88.html

- Instead of being `identifier-syntax` for a syntax object, the keyword names are `identifier-syntax` for keyword objects with appropriate names. (Issue: Do colons get automatically stripped?)
- Both the procedure and the macro created by `define-with-keywords` are able to match keyword arguments provided using the auxiliary syntax keyword, or using literal keyword objects.
- Issue: What do `keyword?` and `keyword=?` do? Do they only work on keyword objects?

## Library Proposal

`(syntax-keywords static)` contains only `define-with-keywords`. `(syntax-keywords dynamic)` also contains `keyword?` and `keyword=?`. (*Mutatis mutandis:* `(srfi xyz static)`, `(scheme keyword static)`, whatever.)

## Acknowledgements

The idea of doing keyword arguments somewhat in this fashion was suggested by Marc Nieper-Wißkirchen. I learned the trick for matching keyword arguments in `syntax-rules` from Alex Shinn’s [`(chibi binary-record)`.](https://github.com/ashinn/chibi-scheme/blob/master/lib/chibi/binary-record.scm) gwatt on Libera Chat #scheme pointed out how to make it work on Chez Scheme by hacking ellipsis renaming into R6RS’s `syntax-rules`.
