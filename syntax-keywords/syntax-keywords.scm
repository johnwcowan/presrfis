;;; Sentinel Values and Macros for Missing Arguments

(define *value-not-given* (list '*value-not-given*))

;; These macros are used as the values of all keyword arguments at the
;; beginning of the ultimate expansion process. If there are any
;; %required-keyword-missing instances left, they will cause a
;; compile-time error, if the Scheme implementation has such a
;; concept.

(define-syntax %required-keyword-missing
  (syntax-rules ()
    ((_ name) (syntax-error "required keyword argument missing" 'name))))
(define-syntax %optional-keyword-missing
  (syntax-rules ()
    ((_ name) *value-not-given*)))

;; Since keyword arguments can come in any order, there might be
;; optional arguments given which, in the formal parameter list, come to
;; the right of other formal parameters which are not given as
;; arguments, so standard opt-lambda or similar won’t work. The
;; sentinel value and this macro allow processing such invocations
;; which, from a purely positional-arguments perspective, have ‘holes’
;; in them.

(define-syntax %let*-disordered-optionals
  (syntax-rules ()
    ((_ ((name gensym initializer) ...) . body)
     (let* ((name (if (eq? gensym *value-not-given*) initializer gensym)) ...)
       . body))))

;;; The Public define-with-keywords Interface
;;
;; The main macro and the other gather macros parse the invocation of
;; define-with-keywords into name, positional arguments, required keyword
;; arguments, optional keyword arguments, and body, and pass these as
;; distinct things into the main macro below. They should correctly
;; refuse to parse invocations where optional arguments appear before
;; required ones.

(define-syntax define-with-keywords
  (syntax-rules ()
    ((_ (name args ...) . body)
     (%define-with-keywords-gather-optionals proc-name name (args ...)
                                       ()
                                       ()
                                       body))
    ((_ proc-name (name args ...) . body)
     (%define-with-keywords-gather-optionals proc-name name (args ...)
                                       ()
                                       ()
                                       body))))


(define-syntax %define-with-keywords-gather-optionals
  (syntax-rules ()
    ((_ proc-name name
        (more-args ... (keyword-name binding-name initializer))
        required-args
        optional-args
        body)
     (%define-with-keywords-gather-optionals proc-name name
                                       (more-args ...)
                                       required-args
                                       ((keyword-name binding-name
                                                      initializer
                                                      gensym)
                                        . optional-args)
                                       body))
    ((_ proc-name name
        (more-args ... (keyword-name binding-name))
        required-args
        optional-args
        body)
     (%define-with-keywords-gather-required proc-name name
                                      (more-args ...)
                                      ((keyword-name binding-name)
                                       . required-args)
                                      optional-args
                                      body))
    ((_ proc-name name (more-args ...) required-args optional-args body)
     (%define-with-keywords-main proc-name name
                           (more-args ...)
                           required-args
                           optional-args
                           body))))

(define-syntax %define-with-keywords-gather-required
  (syntax-rules ()
    ((_ proc-name name
        (more-args ... (keyword-name binding-name))
        required-args
        optional-args
        body)
     (%define-with-keywords-gather-required proc-name name
                                      (more-args ...)
                                      ((keyword-name binding-name)
                                       . required-args)
                                      optional-args
                                      body))
    ((_ proc-name  name (more-args ...) required-args optional-args body)
     (%define-with-keywords-main proc-name name
                           (more-args ...)
                           required-args
                           optional-args
                           body))))

;;; The Main Macro
;;
;; This does the main job of defining the underlying procedure and the
;; macro, though actually generating the syntax transformer is done below.

(define-syntax %define-with-keywords-main
  (syntax-rules ()
    ((_ proc-name name
        (positional-arg ...)
        ((required-keyword-name required-binding-name) ...)
        ((optional-keyword-name optional-binding-name initializer gensym) ...)
        body)
     (begin
       (define-optionals*
         (proc-name positional-arg ...
                    required-binding-name ...
                    (optional-binding-name initializer) ...)
         . body)
       (define (proc/binding positional-arg ...
                             required-binding-name ...
                             gensym ...)
         (%let*-disordered-optionals ((optional-binding-name gensym initializer) ...)
           (proc-name positional-arg ...
                      required-binding-name ...
                      optional-binding-name ...)))
       (%define-with-keywords-build-macro macro/binding proc/binding () ()
                                    ((required-keyword-name required-binding-name) ...
                                     (optional-keyword-name optional-binding-name) ...))
       (define-syntax name
         (syntax-rules ::: (required-keyword-name ...
                            optional-keyword-name ...)
           ((_ positional-arg ... more-args :::)
            (macro/binding proc/binding (more-args :::) (positional-arg ...) ()
                           ((%required-keyword-missing required-keyword-name) ...
                            (%optional-keyword-missing optional-keyword-name) ...)))))))))

;;; The Macro Builder
;;
;; The created macro proceeds through the passed keyword arguments one
;; at once, putting the keyword arguments, which may appear in any
;; order, into an ordered list of the arguments by replacing one item
;; in the list with the the value of the next passed keyword argument
;; on each expansion of the macro.
;;
;; In turn, this macro creates handlers to perform this expansion a
;; step at a time until the list of given arguments is empty. Each
;; step copies all the list items dealt with by previously-created
;; handlers, replaces the list item it’s responsible for with the
;; value of the given keyword argument, and copies the list arguments
;; for handlers which haven’t been created yet. I learned this trick
;; for handling keyword arguments in macros from Alex Shinn (though he
;; may have not actually invented it); my ‘innovation’ is creating a
;; macro-generating macro which creates the keyword-processing macro
;; automatically.
;;
;; If positional arguments are given to keyword parameters, these are
;; put in a separate list, and in the final step of expanding the
;; defined ‘procedure’, they take the place of the keyword arguments
;; until there are none left.

(define-syntax %define-with-keywords-build-macro
  (syntax-rules ()
    ((_ macro/binding proc/binding
        (clause ...)
        ((handled-keyword-name handled-keyword-binding) ...)
        ((this-keyword-name this-keyword-binding)
         (more-keyword-name more-keyword-binding) ...))
     (%define-with-keywords-build-macro
      macro/binding proc/binding
      (clause ...
              ((_ proc/binding
                  (this-keyword-name new-value . more-args)
                  positional-args
                  positional-keyword-args
                  (handled-keyword-binding ...
                   this-keyword-binding
                   more-keyword-binding ...))
               (macro/binding proc/binding more-args positional-args
                              positional-keyword-args
                              (handled-keyword-binding ...
                               new-value
                               more-keyword-binding ...))))
      ((handled-keyword-name handled-keyword-binding) ...
       (this-keyword-name this-keyword-binding))
      ((more-keyword-name more-keyword-binding) ...)))
    ((_ macro/binding proc/binding
        (clause ...) ((handled-keyword-name . more) ...) ())
     (define-syntax macro/binding
       (syntax-rules ::: (handled-keyword-name ...)
         clause ...
         ((_ proc/binding (arg . more-args) positional-args (positional-keyword-args :::) keyword-args)
          (macro/binding proc/binding more-args positional-args (positional-keyword-args ::: arg) keyword-args))
         ((_ proc/binding () (positional-args :::) (positional-keyword-arg . more-positional-keyword-args) (keyword-arg . more-keyword-args))
          (macro/binding proc/binding () (positional-args ::: positional-keyword-arg) more-positional-keyword-args more-keyword-args))
         ((_ proc/binding () (positional-args :::) () (keyword-args :::))
          (proc/binding positional-args ::: keyword-args :::)))))))

;; Local Variables:
;; eval: (put '%let-disordered-optionals 'scheme-indent-function 1)
