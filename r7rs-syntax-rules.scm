;; This code is by gwatt on #scheme — I claim no ownership of it. It
;; implements R7RS’s extended syntax-rules (with ellipsis renaming)
;; for R6RS.

(define-syntax r7rs:syntax-rules
  (lambda (stx)
    (syntax-case stx ()
      [(_ (literals ...) clauses ...)
       #'(syntax-rules (literals ...) clauses ...)]
      [(_ ellipsis (literals ...) clauses ...)
       (identifier? #'ellipsis)
       (letrec ([replace-ellipsis
                  (lambda (stx^)
                    (syntax-case stx^ ()
                      [(a . d)
                       (cons
                         (replace-ellipsis #'a)
                         (replace-ellipsis #'d))]
                      [#(e ...)
                       (list->vector
                         (map replace-ellipsis #'(e ...)))]
                      [x (if (and (identifier? #'x)
                                  (free-identifier=? #'x #'ellipsis))
                             #'(... ...)
                             #'x)]))])
         (replace-ellipsis
           #'(syntax-rules (literals ...) clauses ...)))])))
