# Notes on Strings

## Properties of Internal String Representations

- A UTF-8 bytevector with a table of character offsets allows amortized O(1) `string-ref` and O(n) `string-set!`. `string-set!` may need the bytevector to be resized, making it less efficient than actually reallocating the string. UTF-8 interoperates well with most C libraries one might wish to call through an FFI. UTF-8 is at least as memory-efficient as UTF-16 for text in Latin, Greek, Cyrillic, Hebrew, Arabic, and some other minor scripts, but less efficient for all other scripts.
- A UTF-16 bytevector has the same performance characteristics as a UTF-8 bytevector. Many C libraries support UTF-16, and it is the native string representation of JavaScript, but UTF-16 is not considered by C programmers as often as it used to be.
- A UTF-32 bytevector allows flat O(1) `string-ref` and `string-set!` without a table of character offsets. At least 25% of memory used by strings is wasted for text regardless of the language/script of the text. UTF-32 support in C libraries is comparatively rare.
- ‘Flexible string representation’, i.e. using ISO 8859-1 xor UCS-2 xor UCS-4 depending on what the string actually contains, allows flat O(1) `string-ref`, but has the same performance concerns as UTF-8 and UTF-16 for `string-set!`. The representation is theoretically optimal in memory efficiency, but wastes a lot of space when e.g. a mostly-ASCII text contains a single emoji from the Supplemental Multilingual Pane. Few libraries in C support all three representations.
- Using a rope allows amortized O(log n) `string-ref` and `string-set!`, but may add comparatively high constant factors for small strings. The rope would have to be flattened into a bytevector to pass a string to a library through an FFI.

Tables of character offsets don’t need to cover all characters in a string for amortized O(1) performance. The SRFI 135 sample implementation uses every 128th character.

## String Cursors

[SRFI 130][srfi-130] was voted down on the Red and Tangerine ballots. They seem to just be too much of a pain to work with, not to mention that implementing them efficiently requires implementations to adopt a new pointer tag.

[srfi-130]: https://srfi.schemers.org/srfi-130/srfi-130.html

Also, `string-set!` breaks string cursors. You might as well keep a table of character offsets and just use indexes. Indeed, an efficient-ish implementation of SRFI 13/SRFI 152 is possible in a Scheme implementation with string indexes, I think, by using a table of string cursors, assuming no `string-set!` is used. To be portable, you’d keep a table mapping strings to their respective tables of character offsets. The table would have to have weak keys to be friendly to the garbage collector. It would also be better to build the table lazily, so that it’s only built as far as the last character index that has actually been needed so far.

## Deprecating `string-set!` ?

We are required by WG2’s charter to remain compatible with R7RS small, which by WG1’s charter was, in turn, effectively required to retain `string-set!`. We can’t get rid of `string-set!`, but could we deprecate it like the compatibility procedures in SRFI 125 hash tables?

This wouldn’t get rid of the requirement for it to behave correctly, and for other procedures to behave correctly in its presence, for example.
