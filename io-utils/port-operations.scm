;; (import (scheme case-lambda))
;; (import (srfi 158))

;; Ports, generators, and accumulators

(define input-operation->generator
  ;; it's possible i've misunderstood how SRFI 158 is meant to work,
  ;; in which case these will both be wrong
  (case-lambda
   ((operation) (input-operation->generator operation (current-input-port)))
   ((operation port)
    (lambda ()
      (operation port)))))

(define output-operation->accumulator
  (case-lambda
   ((operation) (output-operation->accumulator operation (current-output-port)))
   ((operation port)
    (lambda (x)
      (if (not (eof-object? x)) (operation x port))))))

;; String and bytevector port operations

;; -- for strings

(define (call-with-input-string string proc)
  (call-with-port (open-input-string string) proc))

(define (call-with-output-string proc)
  (let ((op (open-output-string)))
    (call-with-port op proc)
    (get-output-string op)))

;; PortOperationsCowan claims that these could also be implemented on
;; call-with-port, but R7RS says "It is an error if proc does not
;; accept one argument."
(define (with-input-from-string string thunk)
  (let* ((ip (open-input-string string))
         (res (parameterize ((current-input-port ip)) (thunk))))
    (close-port ip)
    res))

(define (with-output-to-string thunk)
  (let ((op (open-output-string)))
    (parameterize ((current-output-port op))
      (close-port op)
      (get-output-string op))))

;; -- for bytevectors

(define (call-with-input-bytevector bytevector proc)
  (call-with-port (open-input-bytevector bytevector) proc))

(define (call-with-output-string proc)
  (let ((op (open-output-bytevector)))
    (call-with-port op proc)
    (get-output-bytevector op)))

(define (with-input-from-bytevector bytevector thunk)
  (let* ((ip (open-input-bytevector bytevector))
         (res (parameterize ((current-input-port ip)) (thunk))))
    (close-port ip)
    res))

(define (with-output-to-bytevector thunk)
  (let ((op (open-output-bytevector)))
    (parameterize ((current-output-port op))
      (close-port op)
      (get-output-bytevector op))))

;; Convenience I/O operations
(define binary-port-eof?
  (case-lambda
   (() (binary-port-eof? (current-input-port)))
   ((port)
    (eof-object? (peek-u8 port)))))

(define textual-port-eof?
  (case-lambda
   (() (textual-port-eof? (current-input-port)))
   ((port)
    (eof-object? (peek-char port)))))

(define read-lines
  (case-lambda
   (() (read-lines (current-input-port)))
   ((port)
    (generator->list (input-operation->generator read-line port)))))

(define read-all-bytes
  (case-lambda
   (() (read-all-bytes (current-input-port)))
   ((ip)
    (do ((op (open-output-bytevector))
         (chunk "" (read-bytevector 1024 input-port)))
        ((eof-object? chunk) (get-output-bytevector op))
      (write-bytevector chunk output-port)))))

(define read-all-chars
  (case-lambda
   (() (read-all-chars (current-input-port)))
   ((ip)
    (do ((op (open-output-string))
         (chunk "" (read-string 1024 ip)))
        ((eof-object? chunk) (get-output-string op))
      (write-string chunk op)))))

(define read-all
  (case-lambda
   (() (read-lines (current-input-port)))
   ((port)
    (generator->list (input-operation->generator read port)))))

(define write-line
  (case-lambda
   ((string) (write-line string (current-output-port)))
   ((string port)
    (write-string string port)
    (newline port))))

(define (print . objs)
  (for-each display objs)
  (newline))

(define (debug-print-obj obj)
  (if (or (boolean? obj)
          (char? obj)
          (null? obj)
          (symbol? obj)
          (string? obj)
          (and (exact? obj) (integer? obj)))
      (display obj (current-error-port))
      (write obj (current-error-port))))

(define (debug-print . objs)
  (if (null? objs) (newline)
      (begin
        (debug-print-obj (car objs))
        (newline)
        (apply debug-print (cdr objs)))))
