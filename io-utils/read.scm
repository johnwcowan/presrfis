;; Retrospectively annoyed that I didn’t get this into SRFI 207. Mea culpa.
(define (reverse-list->bytevector ls)
  (let ((bv (make-bytevector (length ls))))
    (let next-byte ((idx (- (bytevector-length bv) 1))
                    (head ls))
      (cond ((< idx 0) bv)
            (else
             (bytevector-u8-set! bv idx (car head))
             (next-byte (- idx 1) (cdr head)))))))

;; (read-line* port terminator) where port is an input port, and
;; terminator is either a bytevector or integer ≤ 255, in which case
;; the procedure reads bytes and returns a bytevector, or a string or
;; a character, in which case the procedure reads characters and
;; returns a string.
;;
;; If port is in the EOF state when read-line* is called, it returns
;; an EOF object. Otherwise, it reads from port until it encounters
;; a sequence of characters or bytes matching terminator, or EOF. It
;; then returns a string or bytevector containing all the characters
;; or bytes up to and inclusive of the terminator. If EOF is reached,
;; the returned string or bytevector will not end in the terminator.
(define (read-line* port terminator)
  (cond ((bytevector? terminator)
         (read-binary-line port terminator))
        ((and (integer? terminator)
              (<= terminator 255))
         (read-binary-line port (bytevector terminator)))
        ((string? terminator)
         (read-character-line port terminator))
        ((char? terminator)
         (read-character-line port (string terminator)))
        (else (error "terminator argument to read-line* must be string, character, bytevector, or integer <= 255" terminator))))

(define (read-binary-line port terminator)
  (read-any-line port terminator
                 read-u8 peek-u8
                 =
                 bytevector-u8-ref (bytevector-length terminator)
                 reverse-list->bytevector))

(define (read-character-line port terminator)
  (read-any-line port terminator
                 read-char peek-char
                 char=?
                 string-ref (string-length terminator)
                 reverse-list->string))

(define (read-any-line port terminator
                       read-morsel peek-morsel
                       morsel=?
                       ref terminator-length
                       finalize)
  (if (eof-object? (peek-morsel port)) (eof-object)
      (let read-more ((poss '()) (line '()))
        (let ((m (read-morsel port)))
          (if (eof-object? m)
              (finalize line)
              (let* ((matching-poss
                      (filter (lambda (pos)
                                (morsel=? m (ref terminator pos))) poss))
                     (this-poss (if (morsel=? m (ref terminator 0))
                                    (cons 0 matching-poss)
                                    matching-poss))
                     (next-poss (map (lambda (pos) (+ 1 pos)) this-poss)))
                (if (any
                     (lambda (pos) (= pos terminator-length))
                     next-poss)
                    (finalize (cons m line))
                    (read-more next-poss (cons m line)))))))))
